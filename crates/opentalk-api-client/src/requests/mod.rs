// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

//! Definitions of requests that can be used with the OpenTalk API client.

pub mod auth;
pub mod rooms;
