// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

use chrono::{DateTime, Utc};
use opentalk_types_common::email::EmailAddress;
use serde::Deserialize;

use super::jwt;

/// Service claims
#[derive(Deserialize, Debug)]
pub struct ServiceClaims {
    /// Expires at
    #[serde(with = "time")]
    pub exp: DateTime<Utc>,
    /// Issued at
    #[allow(unused)]
    #[serde(with = "time")]
    pub iat: DateTime<Utc>,
    /// Issuer (URL to the OIDC Provider)
    #[allow(unused)]
    pub iss: String,
    /// Keycloak realm management
    pub realm_access: RealmAccess,
}

impl jwt::VerifyClaims for ServiceClaims {
    fn exp(&self) -> DateTime<Utc> {
        self.exp
    }
}

/// Keycloak realm-management claim which includes the realm specific roles of the client
/// Only included in
#[derive(Deserialize, Debug)]
pub struct RealmAccess {
    pub roles: Vec<String>,
}

/// Claims provided for a logged-in user
//
// A note to devs:
// Please also update fields in `docs/admin/keycloak.md`.
#[derive(Deserialize, Debug)]
pub struct UserClaims {
    /// Expires at
    #[serde(with = "time")]
    pub exp: DateTime<Utc>,
    /// Issued at
    #[allow(unused)]
    #[serde(with = "time")]
    pub iat: DateTime<Utc>,
    /// Issuer (URL to the OIDC Provider)
    pub iss: String,
    /// Subject (User ID)
    pub sub: String,
    /// The users email
    pub email: EmailAddress,
    /// The users firstname
    pub given_name: String,
    /// The users lastname
    pub family_name: String,
    /// The profile picture of the user
    pub picture: Option<String>,
    /// Tenant ID of the user
    pub tenant_id: Option<String>,
    /// Tariff ID of the user
    pub tariff_id: Option<String>,
    /// Tariff status of the user
    pub tariff_status: Option<String>,
    /// Groups the user belongs to.
    /// This is a custom field not specified by the OIDC Standard
    #[serde(default)]
    pub x_grp: Vec<String>,
    /// The users phone number, if configured
    pub phone_number: Option<String>,
    /// The users optional nickname
    pub nickname: Option<String>,
}

impl jwt::VerifyClaims for UserClaims {
    fn exp(&self) -> DateTime<Utc> {
        self.exp
    }
}

mod time {
    use chrono::{DateTime, TimeZone, Utc};
    use serde::{Deserialize, Deserializer};

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let seconds: i64 = Deserialize::deserialize(deserializer)?;

        Utc.timestamp_opt(seconds, 0).single().ok_or_else(|| {
            serde::de::Error::custom(format!(
                "Failed to convert {seconds} seconds to DateTime<Utc>",
            ))
        })
    }
}
